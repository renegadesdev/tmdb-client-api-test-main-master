<?php

namespace Tamasmeszaros\TmdbClientApiTest\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tamasmeszaros\TmdbClientApiTest\Http\Controllers\TmdbApiControllerTestController;
use Tamasmeszaros\TmdbClientApiTest\Tests\TestCase;
use GuzzleHttp\Client;
use Tamasmeszaros\TmdbClientApiTest\Models\Genre;

class TmdbApiClientTest extends TestCase
{
    private $ctrl;

    public function setUp(): void
    {
        parent::setUp();

        config(['tmdb-service.token' => 'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI0YWUyM2ViMzk4OWJlZjAzMTU2NjUwOWY1MmEyNTdmYyIsInN1YiI6IjVmZjA5ZmNkMTc2YTk0MDA0NWU4N2ZlZCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.QuEk3tw2NJZgPqRl8uTAESk_2Dacg9OL1sgc6D3Vr-g']);
        config(['tmdb-service.base_url' => 'https://api.themoviedb.org']);
        config(['tmdb-service.top_rated_movies_endpoint' => 'https://api.themoviedb.org/3/movie/top_rated']);
        config(['tmdb-service.genres_endpoint' => 'https://api.themoviedb.org/3/genre/movie/list']);
        config(['tmdb-service.top_n_movies' => 210]);
        config(['poster_url_base' => 'https://www.themoviedb.org/t/p/w600_and_h900_bestv2']);
        config(['tmdb_url_base' => 'https://www.themoviedb.org/movie/']);
        config(['tmdb-service.concurrency' => 50]);

        $this->ctrl = new TmdbApiControllerTestController();
    }

    public function testGetGenres()
    {
        $this->assertIsArray((new TmdbApiControllerTestController())->getGenres());
    }

    public function testShouldCreateGenre()
    {
        $this->withoutExceptionHandling();

        $record = (new TmdbApiControllerTestController())->createGenres(
            [[
                'id' => 999999,
                'name' => 'Test name'
            ]]
        );

        $record = \Tamasmeszaros\TmdbClientApiTest\Models\Genre::findOrFail(999999);

        $this->assertIsObject($record);

        $this->assertTrue($record->delete());
    }

    public function testGetFirstNTopRatedMovies()
    {
        $this->assertIsArray((new TmdbApiControllerTestController())->getFirstNTopRatedMovies());
        $this->assertCount(config('tmdb-service.top_n_movies'), (new TmdbApiControllerTestController())->getFirstNTopRatedMovies());
    }

    public function testShouldGetMoviesDetails()
    {
        $ctrl = (new TmdbApiControllerTestController());
        $this->assertIsArray($ctrl->getMoviesDetails($ctrl->getFirstNTopRatedMovies())->getData());
    }

    public function testShouldGetMoviesToDirectors()
    {
        $ctrl = (new TmdbApiControllerTestController());

        $this->assertIsArray($ctrl->getMoviesToDirectors($ctrl->getMoviesDetails($ctrl->getFirstNTopRatedMovies())->getData())->toArray());
    }

    public function testShouldCreateMovies()
    {
        $this->withoutExceptionHandling();

        $ctrl = (new TmdbApiControllerTestController());

        $this->assertNull(($ctrl->createMovies($ctrl->getMoviesDetails($ctrl->getFirstNTopRatedMovies())->getData())));
    }

    public function testShouldGetDirectors()
    {
        $this->withoutExceptionHandling();

        $ctrl = (new TmdbApiControllerTestController());
        $this->assertIsArray($ctrl->getDirectors($ctrl->getMoviesToDirectors($ctrl->getMoviesDetails($ctrl->getFirstNTopRatedMovies())->getData())));
    }

    public function testShouldCreateDirectors()
    {
        $this->withoutExceptionHandling();

        $ctrl = (new TmdbApiControllerTestController());

        $this->assertNull($ctrl->createDirectors($ctrl->getDirectorsBio($ctrl->getDirectors($ctrl->getMoviesToDirectors($ctrl->getMoviesDetails($ctrl->getFirstNTopRatedMovies())->getData())))));
    }
}
