<?php

namespace Tamasmeszaros\TmdbClientApiTest\Http\Controllers;

use Illuminate\Routing\Controller;
use GuzzleHttp\Psr7\Response;
use Illuminate\Database\QueryException;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\JsonResponse;
use Illuminate\Support\Facades\Log;
use stdClass;
use Tamasmeszaros\TmdbClientApiTest\Exceptions\TmdbApiException;
use Tamasmeszaros\TmdbClientApiTest\Models\Director;
use Tamasmeszaros\TmdbClientApiTest\Models\Genre;
use Tamasmeszaros\TmdbClientApiTest\Models\Movie;
use Tamasmeszaros\TmdbClientApiTest\Models\MoviesHasDirectors;
use Tamasmeszaros\TmdbClientApiTest\Models\MoviesHasGenres;
use Illuminate\Support\Facades\Schema;
use Exception;

class TmdbApiControllerTestController extends Controller
{
    /**
     * The main entry point of the package. Parse the first 210 movies and store them to the db.
     *
     * @param  mixed $movieIDs
     * @return void
     */
    public function create(array $movieIDs = null)
    {
        try {
            // get the first N movies IDs
            $movies = $movieIDs ?? $this->getFirstNTopRatedMovies();

            // get the movies details
            $moviesDetails = $this->getMoviesDetails($movies)->getData();

            // get the related directors
            $moviesToDirectors = $this->getMoviesToDirectors($moviesDetails);

            // get the directors
            $directors = $this->getDirectors($moviesToDirectors);

            // get the genres
            $genres = $this->getGenres();

            if (!isset($genres['genres'])) {
                throw new TmdbApiException();
            }

            // insert the data into db
            $this->createGenres($genres['genres']);
            $this->createMovies($moviesDetails);
            $this->createDirectors($this->getDirectorsBio($directors));
            $this->createMoviesToDirectors($moviesToDirectors, $moviesDetails);
        } catch (ConnectionException $e) {
            if (app()->runningInConsole()) {
                throw new Exception('Error while connecting to the endpoint. Please check the log for more details.');
            }

            abort(503);
        } catch (TmdbApiException $e) {
            if (app()->runningInConsole()) {
                throw new Exception('API error. Please check the log for more details.');
            }

            abort(503);
        } catch (QueryException $e) {
            if (app()->runningInConsole()) {
                throw new Exception('Database error. Please check the log for more details.');
            }
            abort(500);
        }
    }

    /**
     * Get the genres
     *
     * @return string
     */
    public function getGenres(): array
    {
        try {
            return Http::withToken(config('tmdb-service.token'))
                ->get(config('tmdb-service.genres_endpoint'))
                ->json();
        } catch (ConnectionException $e) {
            throw $e;
        }
    }

    /**
     * Create genres
     *
     * @param  mixed $genres
     * @return void
     */
    public function createGenres($genres): void
    {
        foreach ($genres as $genre) {
            try {
                Genre::updateOrCreate($genre);
            } catch (QueryException $e) {
                Log::error($e->getMessage());
                throw $e;
            }
        }
    }

    /**
     * Create movies
     *
     * @param  mixed $movieDetails
     * @return void
     */
    public function createMovies(array $movieDetails): void
    {
        // parse the spicific infos
        $movies = collect($movieDetails)->mapWithKeys(function ($item) {
            return [$item->id => collect($item)->only(['id', 'title', 'runtime', 'release_date', 'overview', 'poster_path', 'genres', 'vote_average', 'vote_count'])->all()];
        });

        // loop overt the movies collection, collect the values and create the relations
        $movies->each(function ($item, $key) {
            $record = Movie::find($item['id']) ?? new Movie();
            $record->fill($item);

            $record->tmdb_id = $item['id'];
            $record->length = $item['runtime'];
            $record->release_date = $item['release_date'] !== '' ? $item['release_date'] : null;
            $record->poster_url = config('tmdb-service.poster_url_base') . $item['poster_path'];
            $record->tmdb_url = config('tmdb-service.tmdb_url_base') . $item['id'];

            $record->save();

            // remove the current related genres
            MoviesHasGenres::where('movie_id', $record->tmdb_id)->delete();

            foreach ($item['genres'] as $_item) {
                try {
                    MoviesHasGenres::updateOrCreate([
                        'movie_id' => $item['id'],
                        'genre_id' => $_item->id,
                    ]);
                } catch (QueryException $e) {
                    Log::error($e->getMessage());
                    throw $e;
                }
            }
        });
    }

    /**
     * Get the related directors for movies
     *
     * @param  mixed $movieDetails
     * @return Collection
     */
    public function getMoviesToDirectors(array $movieDetails): Collection
    {
        return collect($movieDetails)->mapWithKeys(function ($item) {
            return [$item->id => collect($item->credits->crew)->reject(function ($crew) {
                return $crew->job !== 'Director';
            })];
        });
    }

    /**
     * Create the relation between movies and directors
     *
     * @param  mixed $directors
     * @return void
     */
    public function createMoviesToDirectors($moviesToDirectors, $moviesDetails): void
    {
        $moviesToDirectors->each(function ($item, $key) use ($moviesDetails) {
            if (!sizeof($moviesDetails) &&  ($movie = Movie::find($item['id']))) {
                MoviesHasDirectors::where('movie_id', $item['id'])->delete();
            }

            $item->each(function ($_item, $_key) use ($key) {
                try {
                    MoviesHasDirectors::updateOrCreate([
                        'movie_id' => $key,
                        'director_id' => $_item->id,
                    ]);
                } catch (QueryException $e) {
                    Log::error($e->getMessage());
                }
            });
        });
    }

    /**
     * Get directors
     *
     * @param  mixed $moviesToDirectors
     * @return array
     */
    public function getDirectors($moviesToDirectors): array
    {
        // need to collect the directors to fetch the bio
        $directors = [];

        $moviesToDirectors->each(function ($item, $key) use (&$directors) {
            $item->each(function ($_item, $_key) use (&$directors) {
                $directors[$_item->id] = $_item;
            });
        });

        return $directors;
    }

    /**
     * Create directors
     *
     * @param  mixed $directorsBio
     * @return void
     */
    public function createDirectors($directorsBio): void
    {
        $directorsBio->each(function ($item, $key) {
            try {
                $record = Director::find($item['id']) ?? new Director();

                $record->fill($item);

                $record->tmdb_id = $item['id'];

                $record->save();
            } catch (QueryException $e) {
                Log::error($e->getMessage());
            }
        });
    }

    /**
     * Get the first N movies (ids)
     *
     * @return array
     */
    public function getFirstNTopRatedMovies(): array
    {
        try {
            $topRatedMoviesFirstPage = Http::withToken(config('tmdb-service.token'))
                ->get(config('tmdb-service.top_rated_movies_endpoint'), ['page' => 1])
                ->json();

            $movies = collect([]);

            // put the first 20 movie id to the collection
            $movies->push(collect($topRatedMoviesFirstPage['results'])->map(function ($movie) {
                return $movie['id'];
            })->all());

            $moviesPerPage = sizeof($topRatedMoviesFirstPage['results']);

            $leftoverPagesCt = intval(ceil(config('tmdb-service.top_n_movies') / $moviesPerPage));

            for ($i = 2; $i <= $leftoverPagesCt; $i++) {
                $movies->push(collect(Http::withToken(config('tmdb-service.token'))
                    ->get(config('tmdb-service.top_rated_movies_endpoint'), ['page' => $i])
                    ->json()['results'])->map(function ($movie) {
                    return $movie['id'];
                })->all());
            }

            return $movies->flatten()->take(config('tmdb-service.top_n_movies'))->all();
        } catch (ConnectionException $e) {
            throw $e;
        }
    }

    /**
     * Get movies details
     *
     * @param  mixed $movies
     * @return JsonResponse
     */
    public function getMoviesDetails(array $movies): JsonResponse
    {
        $results = [];
        $headers = [
            'Authorization' => 'Bearer ' . config('tmdb-service.token')
        ];

        $client = new \GuzzleHttp\Client([
            'base_uri' => config('tmdb-service.base_url'),
            'headers' => $headers
        ]);

        $requests = function () use ($client, $headers, $movies) {
            foreach ($movies as $movieId) {
                yield function () use ($client, $movieId, $headers) {
                    return $client->getAsync(
                        '/3/movie/' . $movieId . '?append_to_response=credits'
                    );
                };
            }
        };

        $pool = new \GuzzleHttp\Pool($client, $requests(), [
            'concurrency' => config('tmdb-service.concurrency'),
            'fulfilled' => function (Response $response, $index) use (&$results) {
                $results[] = json_decode($response->getBody(), true);
            },
            'rejected' => function (\GuzzleHttp\Exception\RequestException $reason, $index) {
                Log::error($reason->getMessage());
                throw new TmdbApiException($reason->getMessage());
            },
        ]);

        $pool->promise()->wait();

        return response()->json($results);
    }

    /**
     * Fetch directors
     *
     * @param  mixed $directors
     * @return void
     */
    public function getDirectorsBio($directors)
    {
        $results = [];
        $headers = [
            'Authorization' => 'Bearer ' . config('tmdb-service.token')
        ];

        $client = new \GuzzleHttp\Client([
            'base_uri' => config('tmdb-service.base_url'),
            'headers' => $headers
        ]);

        $requests = function () use ($client, $directors, $headers) {
            foreach ($directors as $directorId => $director) {
                yield function () use ($client, $directorId, $headers) {
                    return $client->getAsync(
                        '/3/person/' . $directorId
                    );
                };
            }
        };

        $pool = new \GuzzleHttp\Pool($client, $requests(), [
            'concurrency' => config('tmdb-service.concurrency'),
            'fulfilled' => function (Response $response, $index) use (&$results) {
                $results[] = json_decode($response->getBody(), true);
            },
            'rejected' => function (\GuzzleHttp\Exception\RequestException $reason, $index) {
                Log::error($reason->getMessage());
                throw new TmdbApiException($reason->getMessage());
            },
        ]);

        $pool->promise()->wait();

        return collect($results)->mapWithKeys(function ($item) {
            return [$item['id'] => collect($item)->only(['id', 'birthday', 'name', 'biography'])->all()];
        });
    }

    /**
     * Update the movies database
     *
     * @return void
     */
    public function update()
    {
        $storedMovieIds = Movie::select('tmdb_id')->pluck('tmdb_id')->toArray();

        $newTopRatedMovies = $this->getFirstNTopRatedMovies();

        // deletable, old movies
        $diff_movies = array_diff($storedMovieIds, $newTopRatedMovies);

        if (sizeof($diff_movies)) {
            Movie::whereIn('tmdb_id', $diff_movies)->delete();
        }

        // insert/update the remaining
        $this->create($newTopRatedMovies);
    }
}
