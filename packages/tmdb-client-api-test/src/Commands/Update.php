<?php

namespace Tamasmeszaros\TmdbClientApiTest\Commands;

use Illuminate\Console\Command;
use Tamasmeszaros\TmdbClientApiTest\Http\Controllers\TmdbApiControllerTestController;
use Exception;

class Update extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tmdb-client-api-test:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the movies database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            (new TmdbApiControllerTestController())->update();
        } catch (Exception $e) {
            echo $e->getMessage() . PHP_EOL;
        }

        return 0;
    }
}
