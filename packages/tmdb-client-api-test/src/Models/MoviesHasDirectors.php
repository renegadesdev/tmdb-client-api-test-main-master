<?php

namespace Tamasmeszaros\TmdbClientApiTest\Models;

use Illuminate\Database\Eloquent\Model;

class MoviesHasDirectors extends Model
{
    /**
     * Indicates if the model's ID is auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    public $timestamps = false;

    public $fillable = [
        'movie_id',
        'director_id'
    ];
}
