<?php

namespace Tamasmeszaros\TmdbClientApiTest\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Movie extends Model
{
    use HasFactory;

    protected $primaryKey = 'tmdb_id';

    /**
     * Indicates if the model's ID is auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    public $fillable = [
        'tmdb_id',
        'title',
        'length',
        'release_date',
        'overview',
        'poster_url',
        'vote_average',
        'vote_count',
        'tmdb_url'
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'release_date' => 'date'
    ];

    public function genres(): BelongsToMany
    {
        return $this->belongsToMany(
            Genre::class,
            'movies_has_genres',
            'movie_id',
            'genre_id'
        );
    }

    public function directors(): BelongsToMany
    {
        return $this->belongsToMany(
            Director::class,
            'movies_has_directors',
            'movie_id',
            'director_id'
        );
    }
}
