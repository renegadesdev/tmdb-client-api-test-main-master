<?php

namespace Tamasmeszaros\TmdbClientApiTest\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MoviesHasGenres extends Model
{
    use HasFactory;

    /**
     * Indicates if the model's ID is auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    public $timestamps = false;

    public $fillable = [
        'movie_id',
        'genre_id'
    ];
}
