<?php

namespace Tamasmeszaros\TmdbClientApiTest\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Director extends Model
{
    use HasFactory;

    protected $primaryKey = 'tmdb_id';

    /**
     * Indicates if the model's ID is auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    public $fillable = [
        'tmdb_id',
        'name',
        'birthday',
        'biography'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'birthday' => 'date:Y-m-d'
    ];
}
