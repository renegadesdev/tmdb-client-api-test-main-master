<?php

namespace Tamasmeszaros\TmdbClientApiTest;

use Illuminate\Support\ServiceProvider;
use Tamasmeszaros\TmdbClientApiTest\Commands\Update;

class TmdbClientApiTestServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/tmdb-service.php',
            'tmdb-service'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                Update::class,
            ]);
        }

        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');

        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        $this->publishes([
            __DIR__ . '/../config/tmdb-service.php' => config_path('tmdb-service.php')
        ]);
    }
}
