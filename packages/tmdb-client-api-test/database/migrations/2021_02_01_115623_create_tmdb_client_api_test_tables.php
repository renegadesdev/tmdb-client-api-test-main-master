<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTmdbClientApiTestTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            //$table->unsignedBigInteger('id');
            $table->bigInteger('tmdb_id')->unsigned()->nullable();
            $table->string('title');
            $table->integer('length')->nullable();
            $table->date('release_date')->nullable();
            $table->longText('overview')->nullable();
            $table->string('poster_url')->nullable();
            $table->decimal('vote_average', 20, 4)->default(0.0);
            $table->integer('vote_count')->default(0);
            $table->text('tmdb_url')->nullable();
            $table->timestamps();

            $table->primary('tmdb_id');
        });

        Schema::create('directors', function (Blueprint $table) {
            //$table->unsignedBigInteger('id');
            $table->unsignedBigInteger('tmdb_id')->nullable();
            $table->string('name');
            $table->date('birthday')->nullable();
            $table->longText('biography')->nullable();
            $table->timestamps();

            $table->primary('tmdb_id');
        });

        Schema::create('genres', function (Blueprint $table) {
            $table->unsignedBigInteger('id');
            $table->string('name');
            $table->timestamps();

            $table->primary('id');
        });

        Schema::create('movies_has_genres', function (Blueprint $table) {
            $table->unsignedBigInteger('movie_id')->nullable();
            $table->foreign('movie_id')->references('tmdb_id')->on('movies')->onDelete('restrict');

            $table->unsignedBigInteger('genre_id')->nullable();
            $table->foreign('genre_id')->references('id')->on('genres')->onDelete('restrict');

            $table->primary(
                ['movie_id', 'genre_id']
            );
        });

        Schema::create('movies_has_directors', function (Blueprint $table) {
            $table->unsignedBigInteger('movie_id')->nullable();
            $table->foreign('movie_id')->references('tmdb_id')->on('movies')->onDelete('restrict');

            $table->unsignedBigInteger('director_id')->nullable();
            $table->foreign('director_id')->references('tmdb_id')->on('directors')->onDelete('restrict');

            $table->primary(
                ['movie_id', 'director_id']
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies_has_genres');
        Schema::dropIfExists('movies_has_directors');
        Schema::dropIfExists('movies');
        Schema::dropIfExists('directors');
        Schema::dropIfExists('genres');
    }
}
