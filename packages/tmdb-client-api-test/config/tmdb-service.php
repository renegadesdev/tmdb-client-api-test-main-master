<?php

return [
    'token' => env('TMDB_TOKEN'),
    'base_url' => env('TMDB_ENDPOINT_BASE', 'https://api.themoviedb.org'),
    'top_rated_movies_endpoint' => env('TMDB_ENDPOINT_TOP_RATED_MOVIES', 'https://api.themoviedb.org/3/movie/top_rated'),
    'genres_endpoint' => env('TMDB_ENDPOINT_GENRES', 'https://api.themoviedb.org/3/genre/movie/list'),
    'top_n_movies' => 210,
    'poster_url_base' => 'https://www.themoviedb.org/t/p/w600_and_h900_bestv2',
    'tmdb_url_base' => 'https://www.themoviedb.org/movie/',
    'concurrency' => 50
];
