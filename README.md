# tmdb-client-api-test

This application is an api client test for TMDB. The application was built in Laravel framework (8.12).

## Requirements

-   PHP ^7.3|^8.0
-   Composer

## Install

First of all, clone the repository:

git clone git@bitbucket.org:dilach/tmdb-client-api-test-main-master.git

or

git clone https://dilach@bitbucket.org/dilach/tmdb-client-api-test-main-master.git

After that, navigate to the right folder:

eg.: cd tmdb-client-api-test-main-master

Rename the ".env.example" file to ".env".

Install the dependencies with the following command:
composer install

In the ".env" file, change the TMDB_TOKEN key's value to your tmdb api token (API Read Access Token (v4 auth)). Currently, it contains my token.

Create a new MySQL database. Change the value of the DB_DATABASE key in the ".env" file to the new database's name and check/modify the credentials.

Generate an application key with the following command:
php artisan key:generate

Run the migrations:
php artisan migrate

After that, you should start a new web server with the following command:
php artisan:serve

## Usage

Navigate to /tmdb-api-client/create route to parse the data in the browser.

## CLI

For the update process, you should use the CLI command:

php artisan tmdb-client-api-test:update

This command is a regular artisan command, so it's schedulable. It will compare the current stored movies to the newly fetched topN movies and will remove the diffence, insert the new ones and update the remains.

## Testing

For testing the package, please navigate to the packages/tmdb-client-api-test folder and run the following command (from CLI):

composer install

and after that:

composer test

In the test/Feautre/TmdbApiClientTest.php test case file, you should change the config values (it contains my credentials - only for testing).
